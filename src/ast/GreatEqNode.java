package ast;

import lib.FOOLlib;

public class GreatEqNode implements Node {

  private Node left;
  private Node right;
  
  public GreatEqNode (Node l, Node r) {
    left=l;
    right=r;
  }
  
  public String toPrint(String s) {
    return s+"GreatEqual\n" + left.toPrint(s+"  ")   
                       + right.toPrint(s+"  ") ; 
  }
  
  public Node typeCheck() {
    Node l = left.typeCheck();
    Node r = right.typeCheck();
    if (! ( FOOLlib.isSubtype(l,r) || FOOLlib.isSubtype(r,l) ) ) {
      System.out.println("Incompatible types in greater/equal");
      System.exit(0);
    }
    if(l instanceof ArrowTypeNode || r instanceof ArrowTypeNode){
		System.out.println("Type error: function in >= !");
		System.exit(0);
	}
    return new BoolTypeNode();
  }  
  
 
	@Override
	public String codeGeneration() {
		String labelA = FOOLlib.freshLabel();
		String labelB = FOOLlib.freshLabel();
				
		return 	right.codeGeneration()+
				left.codeGeneration()+
				"bleq "+labelA+ "\n" +
				left.codeGeneration()+
				right.codeGeneration()+
				"beq "+labelA+ "\n" +
				"push 0\n"+
				"b "+labelB+"\n"+
				labelA+":  \n"+
				"push 1 \n"+
				labelB+":\n";
	}
  
}  