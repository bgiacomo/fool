package ast;

import java.util.ArrayList;

import lib.FOOLlib;

public class FunNode implements Node, DecNode {

	private String id;
	private Node returnValueType;
	private ArrayList<Node> parlist;
	private ArrayList<Node> declist;
	private Node body;
	private Node symType;

	public FunNode(String i, Node t) {
		id = i;
		returnValueType = t;
		parlist = new ArrayList<Node>();
		declist = new ArrayList<Node>();
	}

	public void addDec(ArrayList<Node> d) {
		declist = d;
	}
	public void addBody(Node b){
		this.body=b;
	}

	public void addPar(Node p) {
		parlist.add(p);
	}

	public void addSymType(Node functionType) {
		this.symType = functionType;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		String declstr = "";
		if (declist != null)
			for (Node dec : declist)
				declstr += dec.toPrint(s + "  ");
		return s + "Fun:" + id + "\n" + returnValueType.toPrint(s + "  ") + parlstr + declstr + body.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {

	
		/*controllo del valore di ritorno*/
		if (!FOOLlib.isSubtype(this.body.typeCheck(), returnValueType)) {
			System.out.println("Wrong return type for function " + this.id);
			System.exit(0);

		}
		if (!declist.isEmpty()){
			for (Node node : this.declist) {
				node.typeCheck();
			}
		}
		return this.returnValueType;
	}
	
	

	public String codeGeneration() {
		String declarationsCode = "";
			for (Node dec : declist){
				declarationsCode += dec.codeGeneration();
	}
		String popDecl = "";
			for (Node dec : declist) {
				popDecl += "pop\n";
				/* se la dichiarazione ha tipo funzione devo eliminare sia
				 l'indirizzo che il suo FP*/
				if (((DecNode) dec).getSymType() instanceof ArrowTypeNode) {
					popDecl += "pop\n";
				}
			}

		/*
		 * Creo i pop per la deallocazione dei parametri
		 */
		String popParl = "";
		for (Node par : parlist) {
			popParl += "pop\n";
			if (((DecNode) par).getSymType() instanceof ArrowTypeNode) {
				popParl += "pop\n";
			}
		}
		String functionLabel = FOOLlib.freshFunLabel();
		FOOLlib.putCode(functionLabel + ":\n" + "cfp\n" + // setta $fp a $sp
				"lra\n" + 								// inserimento return address: il valore di ip per
														// ritornare al codice
				declarationsCode + 						// inserimento dichiarazioni locali
				body.codeGeneration() + 				// exp, la parte dopo in
				"srv\n" + 								// viene rimosso dalla stack il valore di ritorno con una pop e salvato nel registro
				popDecl + "sra\n" + 					// pop del return address
				"pop\n" + 								// pop di AL
				popParl + "sfp\n" + 					// setto $fp a valore del CL
				"lrv\n" + 								// risultato della funzione sullo stack"
				"lra\n" + "js\n" 						// salta a $ra
		);

		return "lfp\n" + "push " + functionLabel + "\n";//devo mettero il fp corrente e l'indirizzo a cui saltare
	}
	

	@Override
	public Node getSymType() {
		// TODO Auto-generated method stub
		return this.symType;
	}

}