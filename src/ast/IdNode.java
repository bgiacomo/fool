	package ast;

public class IdNode implements Node {

	private String id;
	private STentry entry;
	private int nestinglevel;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		entry = st;
		nestinglevel = nl;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestlev " + nestinglevel + "\n" + entry.toPrint(s + "  ");
	}

	public Node typeCheck() {
//ora puo' essere anche funzionale
		return entry.getType();
	}

	public String codeGeneration() {
		String getAR = "";
		for (int i = 0; i < nestinglevel - entry.getNestinglevel(); i++)
			getAR += "lw\n";	
		if (entry.getType() instanceof ArrowTypeNode) {
			return "push "+this.entry.getOffset()+"\n"+
					"lfp\n"+
					getAR+
					"add\n"+
					"lw\n"+ //recupero il valore del FP
					
					"push "+(this.entry.getOffset()-1)+"\n"+	//recupero l'indirizzo della funzione


					"lfp\n" + getAR + // risalgo la catena statica e metto fp AR
										// dichiarazione
														// secondo il nostro
														// layput, il RA �
														// subito sotto, quindi
														// l'indirizzo
					"add\n" + 							// � ad offset -1. carico sulla
														// stack il valore a
														// quell'indirizzo
					"lw\n"

			;

		} else {
 			return "push " + entry.getOffset() + "\n" + // calcola indirizzo
														// offset+$fp
					"lfp\n" + getAR + // risalgo la catena statica
					"add\n" + "lw\n"; // carica sullo stack il valore a
										// quell'indirizzo
		}
	}
}
