	package ast;

import java.util.ArrayList;

import lib.FOOLlib;

/**
 * @author OPeratore
 *
 */
public class CallNode implements Node {

	private String id;
	private STentry entry;
	private ArrayList<Node> parlist;
	private int nestinglevel;

	public CallNode(String i, STentry e, ArrayList<Node> p, int nl) {
		id = i;
		entry = e;
		parlist = p;
		nestinglevel = nl;
	}

	public String toPrint(String s) { //
		String parlstr = "";
		for (Node par : parlist)
			parlstr += par.toPrint(s + "  ");
		return s + "Call:" + id + " at nestlev " + nestinglevel + "\n" + entry.toPrint(s + "  ") + parlstr;
	}

	public Node typeCheck() { //
		ArrowTypeNode arrowType = null;
		if (entry.getType() instanceof ArrowTypeNode)
			arrowType = (ArrowTypeNode) entry.getType();
		else {
			System.out.println("Invocation of a non-function " + id);
			System.exit(0);
		}
		ArrayList<Node> parList = arrowType.getParList();
		if (parList.size() != parlist.size()) {
			System.out.println("Wrong number of parameters in the invocation of " + id);
			System.exit(0);
		}
		for (int i = 0; i < parlist.size(); i++)
			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), parList.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		return arrowType.getRet();
	}


	/*Quando trova un ID di funzione, viene messo sulla stack l'Activation Record
	 * 
	 * Prima viene settato il Control Link come valore corrente del frame pointer
	 * Vengono posizionati i parametri dall'ultimo al primo
	 * Viene recuperato l'Address Link con il meccanismo di risalita statica
	 * L'indirizzo a cui bisogna fare il salto con "js" viene recuperato facendo nuovamente la risalita statica
	 * ma ad "offset-1" perch� le funzioni occupano due celle di memoria 
	 *  
	 * */
	public String codeGeneration(){
		
		String parCode = "";
		String getAR = "";
		for (int i = 0; i < nestinglevel - entry.getNestinglevel(); i++)
			getAR += "lw\n";
		for (int i = parlist.size() - 1; i >= 0; i--) {
				parCode += parlist.get(i).codeGeneration();				
		}
		
		return "lfp\n"+ 
				parCode+
				"push "+entry.getOffset()+"\n"+
				"lfp\n"+
				getAR+
				"add\n"+
				"lw\n"+
				"push "+(entry.getOffset()-1)+"\n"+
				"lfp\n"+
				getAR+
				"add\n"+
				"lw\n"+
				"js\n";
				
				
		
	}




}
