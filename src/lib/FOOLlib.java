package lib;

import java.util.ArrayList;

import ast.*;

public class FOOLlib {

	private static int labCount = 0;

	private static int funLabCount = 0;

	private static String funCode = "";

	public static boolean isSubtype(Node a, Node b) {

		boolean result;

		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			//  co-varianza 

			Node returnTypeA = ((ArrowTypeNode) a).getRet();
			Node returnTypeB = ((ArrowTypeNode) b).getRet();
			boolean resultType = FOOLlib.isSubtype(returnTypeA, returnTypeB);

			// contro-varianza 
			ArrayList<Node> aParList = ((ArrowTypeNode) a).getParList();
			ArrayList<Node> bParList = ((ArrowTypeNode) b).getParList();

			boolean result_par = true;

			for (int i = 0; i < aParList.size(); i++) {
				result_par &= FOOLlib.isSubtype(bParList.get(i), aParList.get(i));
			}

			result = resultType && result_par;

		}

		else {
			// controllo normale
			result = a.getClass().equals(b.getClass()) || (a instanceof BoolTypeNode && b instanceof IntTypeNode);

		}
		return result;

	}

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	public static String freshFunLabel() {
		return "function" + (funLabCount++);
	}

	public static void putCode(String c) {
		funCode += "\n" + c;
	}

	public static String getCode() {
		return funCode;
	}

}