// $ANTLR 3.5.2 C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g 2017-05-25 16:55:09

import java.util.ArrayList;
import java.util.HashMap;
import ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", 
		"CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", 
		"ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GE", "ID", "IF", "IN", 
		"INT", "INTEGER", "LE", "LET", "LPAR", "MINUS", "NEW", "NOT", "NULL", 
		"OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TIMES", "TRUE", "VAR", 
		"WHITESP"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g"; }


	private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();

	private int nestingLevel = -1;
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)



	// $ANTLR start "prog"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:25:1: prog returns [Node ast] : (e= exp SEMIC | LET d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> d =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:26:3: (e= exp SEMIC | LET d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||LA1_0==NOT||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:26:11: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog49);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog51); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:28:11: LET d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog78); 
					nestingLevel++;
					             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
					             symTable.add(hm);
					            
					pushFollow(FOLLOW_declist_in_prog107);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog109); 
					pushFollow(FOLLOW_exp_in_prog113);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog115); 
					symTable.remove(nestingLevel--);
					             ast = new ProgLetInNode(d,e) ;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "declist"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:39:1: declist returns [ArrayList<Node> astlist] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<Node> d =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:40:3: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:40:5: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{
			astlist = new ArrayList<Node>() ;
			     int offset=-2;
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:42:9: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==FUN||LA6_0==VAR) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:42:11: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:42:11: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==VAR) ) {
						alt5=1;
					}
					else if ( (LA5_0==FUN) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:43:13: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist177); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist181); 
							match(input,COLON,FOLLOW_COLON_in_declist183); 
							pushFollow(FOLLOW_type_in_declist187);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist190); 
							pushFollow(FOLLOW_exp_in_declist194);
							e=exp();
							state._fsp--;

							VarNode v = new VarNode((i!=null?i.getText():null),t,e); 
							               astlist.add(v);
							               HashMap<String,STentry> hm = symTable.get(nestingLevel); 
							               if ( hm.put((i!=null?i.getText():null),new STentry(nestingLevel,t,offset--)) != null  ) // t
							                 {System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                  System.exit(0);}  
							              
							}
							break;
						case 2 :
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:52:13: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist239); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist243); 
							match(input,COLON,FOLLOW_COLON_in_declist245); 
							pushFollow(FOLLOW_type_in_declist249);
							t=type();
							state._fsp--;

							//inserimento di ID nella symtable
							               FunNode f = new FunNode((i!=null?i.getText():null),t);
							               astlist.add(f);
							               HashMap<String,STentry> hm = symTable.get(nestingLevel);
							              //nell'higherorder layout ogni unziona occupa un offset doppio!
							               STentry entry = new STentry(nestingLevel,offset/*offset per l'higher order decrementato di due*/); //separo introducendo "entry"
							               offset-=2;                
							               if ( hm.put((i!=null?i.getText():null),entry) != null )
							                 {System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                  System.exit(0);}
							                  //creare una nuova hashmap per la symTable
							               nestingLevel++;
							               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
							               symTable.add(hmn);
							               ArrayList<Node> parTypes = new ArrayList<Node>();
							               int paroffset=1;
							              
							match(input,LPAR,FOLLOW_LPAR_in_declist281); 
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:71:17: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt3=2;
							int LA3_0 = input.LA(1);
							if ( (LA3_0==ID) ) {
								alt3=1;
							}
							switch (alt3) {
								case 1 :
									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:71:18: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
									{
									fid=(Token)match(input,ID,FOLLOW_ID_in_declist303); 
									match(input,COLON,FOLLOW_COLON_in_declist305); 
									pushFollow(FOLLOW_type_in_declist309);
									fty=type();
									state._fsp--;

									 
									                  parTypes.add(fty); //
									                  ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty);
									                  f.addPar(fpar);
									                  //nel caso in cui ci siano paramtetri funzionali devo riservare due spazi
									                  if(fty instanceof ArrowTypeNode){
									                  paroffset++;
									                  }
									                  
									                  //se non è funzionale, semplicemente incremento di uno!
									                  if ( hmn.put((fid!=null?fid.getText():null),new STentry(nestingLevel,fty,paroffset++)) != null  )
									                    {System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
									                     System.exit(0);}
									                  
									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:86:19: ( COMMA id= ID COLON ty= type )*
									loop2:
									while (true) {
										int alt2=2;
										int LA2_0 = input.LA(1);
										if ( (LA2_0==COMMA) ) {
											alt2=1;
										}

										switch (alt2) {
										case 1 :
											// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:86:20: COMMA id= ID COLON ty= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist350); 
											id=(Token)match(input,ID,FOLLOW_ID_in_declist354); 
											match(input,COLON,FOLLOW_COLON_in_declist356); 
											pushFollow(FOLLOW_type_in_declist360);
											ty=type();
											state._fsp--;


											                      if(ty instanceof ArrowTypeNode){ //come prima, se il parametro è funzionale, deve occupare due spazi  
											                    
											                       paroffset++;
											                       
											                        }
											                    
											                    parTypes.add(ty); //
											                    ParNode par = new ParNode((id!=null?id.getText():null),ty);
											                    f.addPar(par);
											                    if ( hmn.put((id!=null?id.getText():null),new STentry(nestingLevel,ty,paroffset++)) != null  )
											                      {System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
											                       System.exit(0);}
											                    
											}
											break;

										default :
											break loop2;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist439); 
							ArrowTypeNode functionType = new ArrowTypeNode(parTypes,t);
							                entry.addType(functionType);
							            // aggiungo il tipo anche al FunNode
							              f.addSymType(functionType);
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:107:15: ( LET d= declist IN )?
							int alt4=2;
							int LA4_0 = input.LA(1);
							if ( (LA4_0==LET) ) {
								alt4=1;
							}
							switch (alt4) {
								case 1 :
									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:107:16: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist459); 
									pushFollow(FOLLOW_declist_in_declist463);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist465); 
									f.addDec(d);
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist472);
							e=exp();
							state._fsp--;

							//chiudere scope
							              symTable.remove(nestingLevel--);
							              f.addBody(e);
							              
							              
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist503); 
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "type"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:118:1: type returns [Node ast] : (b= basic |a= arrow );
	public final Node type() throws RecognitionException {
		Node ast = null;


		Node b =null;
		Node a =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:118:27: (b= basic |a= arrow )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==BOOL||LA7_0==INT) ) {
				alt7=1;
			}
			else if ( (LA7_0==LPAR) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:118:30: b= basic
					{
					pushFollow(FOLLOW_basic_in_type547);
					b=basic();
					state._fsp--;

					ast =b;
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:119:11: a= arrow
					{
					pushFollow(FOLLOW_arrow_in_type562);
					a=arrow();
					state._fsp--;

					ast =a;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "basic"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:122:1: basic returns [Node ast] : ( INT | BOOL );
	public final Node basic() throws RecognitionException {
		Node ast = null;


		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:122:27: ( INT | BOOL )
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==INT) ) {
				alt8=1;
			}
			else if ( (LA8_0==BOOL) ) {
				alt8=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}

			switch (alt8) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:122:29: INT
					{
					match(input,INT,FOLLOW_INT_in_basic587); 
					ast =new IntTypeNode();
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:123:11: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_basic615); 
					ast =new BoolTypeNode();
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "basic"



	// $ANTLR start "arrow"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:127:1: arrow returns [Node ast] : LPAR (t= type ( COMMA t= type )* )? RPAR ARROW b= basic ;
	public final Node arrow() throws RecognitionException {
		Node ast = null;


		Node t =null;
		Node b =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:127:27: ( LPAR (t= type ( COMMA t= type )* )? RPAR ARROW b= basic )
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:128:3: LPAR (t= type ( COMMA t= type )* )? RPAR ARROW b= basic
			{

			    //lista dei parametri
			    ArrayList<Node> parList = new ArrayList<Node>();
			  
			match(input,LPAR,FOLLOW_LPAR_in_arrow681); 
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:132:8: (t= type ( COMMA t= type )* )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0==BOOL||LA10_0==INT||LA10_0==LPAR) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:132:9: t= type ( COMMA t= type )*
					{
					pushFollow(FOLLOW_type_in_arrow686);
					t=type();
					state._fsp--;

					parList.add(t);
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:132:39: ( COMMA t= type )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==COMMA) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:132:40: COMMA t= type
							{
							match(input,COMMA,FOLLOW_COMMA_in_arrow691); 
							pushFollow(FOLLOW_type_in_arrow695);
							t=type();
							state._fsp--;

							parList.add(t);
							}
							break;

						default :
							break loop9;
						}
					}

					}
					break;

			}

			match(input,RPAR,FOLLOW_RPAR_in_arrow704); 
			match(input,ARROW,FOLLOW_ARROW_in_arrow706); 
			pushFollow(FOLLOW_basic_in_arrow710);
			b=basic();
			state._fsp--;

			ast = new ArrowTypeNode(parList,b);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "arrow"



	// $ANTLR start "exp"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:137:1: exp returns [Node ast] : f= term ( PLUS l= term | MINUS l2= term | OR l4= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;
		Node l2 =null;
		Node l4 =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:138:3: (f= term ( PLUS l= term | MINUS l2= term | OR l4= term )* )
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:138:5: f= term ( PLUS l= term | MINUS l2= term | OR l4= term )*
			{
			pushFollow(FOLLOW_term_in_exp752);
			f=term();
			state._fsp--;

			ast = f;
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:139:7: ( PLUS l= term | MINUS l2= term | OR l4= term )*
			loop11:
			while (true) {
				int alt11=4;
				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt11=1;
					}
					break;
				case MINUS:
					{
					alt11=2;
					}
					break;
				case OR:
					{
					alt11=3;
					}
					break;
				}
				switch (alt11) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:139:8: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp763); 
					pushFollow(FOLLOW_term_in_exp767);
					l=term();
					state._fsp--;

					ast = new PlusNode (ast,l);
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:142:9: MINUS l2= term
					{
					match(input,MINUS,FOLLOW_MINUS_in_exp799); 
					pushFollow(FOLLOW_term_in_exp803);
					l2=term();
					state._fsp--;

					ast = new MinNode (ast,l2);
					}
					break;
				case 3 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:145:9: OR l4= term
					{
					match(input,OR,FOLLOW_OR_in_exp834); 
					pushFollow(FOLLOW_term_in_exp838);
					l4=term();
					state._fsp--;

					ast =new OrNode(ast,l4);
					}
					break;

				default :
					break loop11;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:149:1: term returns [Node ast] : f= factor ( TIMES l= factor | DIV l2= factor | AND l5= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;
		Node l2 =null;
		Node l5 =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:150:3: (f= factor ( TIMES l= factor | DIV l2= factor | AND l5= factor )* )
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:150:5: f= factor ( TIMES l= factor | DIV l2= factor | AND l5= factor )*
			{
			pushFollow(FOLLOW_factor_in_term871);
			f=factor();
			state._fsp--;

			ast = f;
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:151:7: ( TIMES l= factor | DIV l2= factor | AND l5= factor )*
			loop12:
			while (true) {
				int alt12=4;
				switch ( input.LA(1) ) {
				case TIMES:
					{
					alt12=1;
					}
					break;
				case DIV:
					{
					alt12=2;
					}
					break;
				case AND:
					{
					alt12=3;
					}
					break;
				}
				switch (alt12) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:151:8: TIMES l= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term882); 
					pushFollow(FOLLOW_factor_in_term886);
					l=factor();
					state._fsp--;

					ast = new MultNode (ast,l);
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:154:9: DIV l2= factor
					{
					match(input,DIV,FOLLOW_DIV_in_term916); 
					pushFollow(FOLLOW_factor_in_term920);
					l2=factor();
					state._fsp--;

					ast = new DivNode (ast,l2);
					}
					break;
				case 3 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:157:9: AND l5= factor
					{
					match(input,AND,FOLLOW_AND_in_term951); 
					pushFollow(FOLLOW_factor_in_term955);
					l5=factor();
					state._fsp--;

					ast =new AndNode(ast,l5);
					}
					break;

				default :
					break loop12;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:161:1: factor returns [Node ast] : f= value ( EQ l= value | GE l3= value | LE l2= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;
		Node l3 =null;
		Node l2 =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:162:3: (f= value ( EQ l= value | GE l3= value | LE l2= value )* )
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:162:5: f= value ( EQ l= value | GE l3= value | LE l2= value )*
			{
			pushFollow(FOLLOW_value_in_factor989);
			f=value();
			state._fsp--;

			ast = f;
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:163:7: ( EQ l= value | GE l3= value | LE l2= value )*
			loop13:
			while (true) {
				int alt13=4;
				switch ( input.LA(1) ) {
				case EQ:
					{
					alt13=1;
					}
					break;
				case GE:
					{
					alt13=2;
					}
					break;
				case LE:
					{
					alt13=3;
					}
					break;
				}
				switch (alt13) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:163:8: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor1000); 
					pushFollow(FOLLOW_value_in_factor1004);
					l=value();
					state._fsp--;

					ast = new EqualNode (ast,l);
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:166:10: GE l3= value
					{
					match(input,GE,FOLLOW_GE_in_factor1034); 
					pushFollow(FOLLOW_value_in_factor1038);
					l3=value();
					state._fsp--;

					ast =new GreatEqNode(ast,l3);
					}
					break;
				case 3 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:168:10: LE l2= value
					{
					match(input,LE,FOLLOW_LE_in_factor1063); 
					pushFollow(FOLLOW_value_in_factor1067);
					l2=value();
					state._fsp--;

					ast =new LessEqNode(ast,l2);
					}
					break;

				default :
					break loop13;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:172:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | NOT LPAR not= exp RPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT ID LPAR ( exp ( COMMA exp )* )? RPAR )? );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node not =null;
		Node fa =null;
		Node a =null;

		try {
			// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:173:3: (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | NOT LPAR not= exp RPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT ID LPAR ( exp ( COMMA exp )* )? RPAR )? )
			int alt19=8;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt19=1;
				}
				break;
			case TRUE:
				{
				alt19=2;
				}
				break;
			case FALSE:
				{
				alt19=3;
				}
				break;
			case LPAR:
				{
				alt19=4;
				}
				break;
			case IF:
				{
				alt19=5;
				}
				break;
			case NOT:
				{
				alt19=6;
				}
				break;
			case PRINT:
				{
				alt19=7;
				}
				break;
			case ID:
				{
				alt19=8;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}
			switch (alt19) {
				case 1 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:173:5: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value1112); 
					ast = new IntNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:175:5: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value1129); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:177:5: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value1145); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:179:5: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value1159); 
					pushFollow(FOLLOW_exp_in_value1163);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1165); 
					ast = e;
					}
					break;
				case 5 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:181:5: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value1179); 
					pushFollow(FOLLOW_exp_in_value1183);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value1185); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1187); 
					pushFollow(FOLLOW_exp_in_value1191);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1193); 
					match(input,ELSE,FOLLOW_ELSE_in_value1203); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1205); 
					pushFollow(FOLLOW_exp_in_value1209);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1211); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 6 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:184:5: NOT LPAR not= exp RPAR
					{
					match(input,NOT,FOLLOW_NOT_in_value1226); 
					match(input,LPAR,FOLLOW_LPAR_in_value1228); 
					pushFollow(FOLLOW_exp_in_value1232);
					not=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1234); 
					ast =new NotNode(not);
					}
					break;
				case 7 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:187:5: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value1250); 
					match(input,LPAR,FOLLOW_LPAR_in_value1252); 
					pushFollow(FOLLOW_exp_in_value1256);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1258); 
					ast = new PrintNode(e);
					}
					break;
				case 8 :
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:189:5: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT ID LPAR ( exp ( COMMA exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value1273); 
					//cercare la dichiarazione
					    int j=nestingLevel;
					    STentry entry=null; 
					    
					    while (j>=0 && entry==null){
					      entry=(symTable.get(j--)).get((i!=null?i.getText():null));
					     
					      }
					      
					    if (entry==null)
					      {System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared");
					       System.exit(0);}               
					    ast = new IdNode((i!=null?i.getText():null),entry,nestingLevel);
					   
					// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:205:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT ID LPAR ( exp ( COMMA exp )* )? RPAR )?
					int alt18=3;
					int LA18_0 = input.LA(1);
					if ( (LA18_0==LPAR) ) {
						alt18=1;
					}
					else if ( (LA18_0==DOT) ) {
						alt18=2;
					}
					switch (alt18) {
						case 1 :
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:205:7: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value1293); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:206:7: (fa= exp ( COMMA a= exp )* )?
							int alt15=2;
							int LA15_0 = input.LA(1);
							if ( (LA15_0==FALSE||(LA15_0 >= ID && LA15_0 <= IF)||LA15_0==INTEGER||LA15_0==LPAR||LA15_0==NOT||LA15_0==PRINT||LA15_0==TRUE) ) {
								alt15=1;
							}
							switch (alt15) {
								case 1 :
									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:206:8: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1307);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:207:9: ( COMMA a= exp )*
									loop14:
									while (true) {
										int alt14=2;
										int LA14_0 = input.LA(1);
										if ( (LA14_0==COMMA) ) {
											alt14=1;
										}

										switch (alt14) {
										case 1 :
											// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:207:10: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1320); 
											pushFollow(FOLLOW_exp_in_value1324);
											a=exp();
											state._fsp--;

											argList.add(a);
											}
											break;

										default :
											break loop14;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1353); 
							ast =new CallNode((i!=null?i.getText():null),entry,argList,nestingLevel);
							}
							break;
						case 2 :
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:210:10: DOT ID LPAR ( exp ( COMMA exp )* )? RPAR
							{
							match(input,DOT,FOLLOW_DOT_in_value1366); 
							match(input,ID,FOLLOW_ID_in_value1368); 
							match(input,LPAR,FOLLOW_LPAR_in_value1370); 
							// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:210:22: ( exp ( COMMA exp )* )?
							int alt17=2;
							int LA17_0 = input.LA(1);
							if ( (LA17_0==FALSE||(LA17_0 >= ID && LA17_0 <= IF)||LA17_0==INTEGER||LA17_0==LPAR||LA17_0==NOT||LA17_0==PRINT||LA17_0==TRUE) ) {
								alt17=1;
							}
							switch (alt17) {
								case 1 :
									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:210:23: exp ( COMMA exp )*
									{
									pushFollow(FOLLOW_exp_in_value1373);
									exp();
									state._fsp--;

									// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:210:27: ( COMMA exp )*
									loop16:
									while (true) {
										int alt16=2;
										int LA16_0 = input.LA(1);
										if ( (LA16_0==COMMA) ) {
											alt16=1;
										}

										switch (alt16) {
										case 1 :
											// C:\\Users\\OPeratore\\git\\FOOL_FINALE\\FOOL.g:210:28: COMMA exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1376); 
											pushFollow(FOLLOW_exp_in_value1378);
											exp();
											state._fsp--;

											}
											break;

										default :
											break loop16;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1385); 
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog49 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog51 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog78 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_prog107 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_prog109 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_prog113 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog115 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_declist177 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist181 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist183 = new BitSet(new long[]{0x0000000044000080L});
	public static final BitSet FOLLOW_type_in_declist187 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_declist190 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_declist194 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_FUN_in_declist239 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist243 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist245 = new BitSet(new long[]{0x0000000044000080L});
	public static final BitSet FOLLOW_type_in_declist249 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_declist281 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_declist303 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist305 = new BitSet(new long[]{0x0000000044000080L});
	public static final BitSet FOLLOW_type_in_declist309 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_declist350 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist354 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist356 = new BitSet(new long[]{0x0000000044000080L});
	public static final BitSet FOLLOW_type_in_declist360 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_declist439 = new BitSet(new long[]{0x0000042269900000L});
	public static final BitSet FOLLOW_LET_in_declist459 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_declist463 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_declist465 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_declist472 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist503 = new BitSet(new long[]{0x0000080000200002L});
	public static final BitSet FOLLOW_basic_in_type547 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrow_in_type562 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_basic587 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_basic615 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_arrow681 = new BitSet(new long[]{0x0000004044000080L});
	public static final BitSet FOLLOW_type_in_arrow686 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_arrow691 = new BitSet(new long[]{0x0000000044000080L});
	public static final BitSet FOLLOW_type_in_arrow695 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_arrow704 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ARROW_in_arrow706 = new BitSet(new long[]{0x0000000004000080L});
	public static final BitSet FOLLOW_basic_in_arrow710 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_exp752 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_PLUS_in_exp763 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_term_in_exp767 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_MINUS_in_exp799 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_term_in_exp803 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_OR_in_exp834 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_term_in_exp838 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_factor_in_term871 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_TIMES_in_term882 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_factor_in_term886 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_DIV_in_term916 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_factor_in_term920 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_AND_in_term951 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_factor_in_term955 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_value_in_factor989 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_EQ_in_factor1000 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_value_in_factor1004 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_GE_in_factor1034 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_value_in_factor1038 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_LE_in_factor1063 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_value_in_factor1067 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_INTEGER_in_value1112 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value1129 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value1145 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value1159 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1163 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1165 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value1179 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1183 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_THEN_in_value1185 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1187 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1191 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1193 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_value1203 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1205 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1209 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1211 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_value1226 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1228 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1232 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1234 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value1250 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1252 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1256 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1258 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value1273 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_LPAR_in_value1293 = new BitSet(new long[]{0x0000046249900000L});
	public static final BitSet FOLLOW_exp_in_value1307 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1320 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1324 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1353 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_value1366 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_value1368 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1370 = new BitSet(new long[]{0x0000046249900000L});
	public static final BitSet FOLLOW_exp_in_value1373 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1376 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1378 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1385 = new BitSet(new long[]{0x0000000000000002L});
}
