// $ANTLR 3.5.2 /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g 2017-05-24 21:43:53

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	int lexicalErrors=0;


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FOOLLexer() {} 
	public FOOLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g"; }

	// $ANTLR start "CLASS"
	public final void mCLASS() throws RecognitionException {
		try {
			int _type = CLASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:220:7: ( 'class' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:220:9: 'class'
			{
			match("class"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLASS"

	// $ANTLR start "EXTENDS"
	public final void mEXTENDS() throws RecognitionException {
		try {
			int _type = EXTENDS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:221:9: ( 'extends' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:221:11: 'extends'
			{
			match("extends"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXTENDS"

	// $ANTLR start "NEW"
	public final void mNEW() throws RecognitionException {
		try {
			int _type = NEW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:222:7: ( 'new' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:222:9: 'new'
			{
			match("new"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEW"

	// $ANTLR start "NULL"
	public final void mNULL() throws RecognitionException {
		try {
			int _type = NULL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:223:9: ( 'null' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:223:11: 'null'
			{
			match("null"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NULL"

	// $ANTLR start "DOT"
	public final void mDOT() throws RecognitionException {
		try {
			int _type = DOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:224:4: ( '.' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:224:6: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOT"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			int _type = LE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:225:3: ( '<=' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:225:5: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LE"

	// $ANTLR start "GE"
	public final void mGE() throws RecognitionException {
		try {
			int _type = GE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:226:3: ( '>=' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:226:4: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GE"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:227:4: ( '&&' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:227:6: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:228:3: ( '||' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:228:5: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:229:4: ( 'not' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:229:6: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:230:4: ( '/' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:230:6: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:231:6: ( '-' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:231:7: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			int _type = SEMIC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:232:7: ( ';' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:232:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMIC"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:233:7: ( ':' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:233:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:234:7: ( ',' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:234:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:235:5: ( '==' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:235:7: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "ASS"
	public final void mASS() throws RecognitionException {
		try {
			int _type = ASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:236:5: ( '=' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:236:7: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASS"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:237:7: ( '+' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:237:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "TIMES"
	public final void mTIMES() throws RecognitionException {
		try {
			int _type = TIMES;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:238:7: ( '*' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:238:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TIMES"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:9: ( ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='-'||(LA3_0 >= '1' && LA3_0 <= '9')) ) {
				alt3=1;
			}
			else if ( (LA3_0=='0') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:11: ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* )
					{
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:11: ( '-' )?
					int alt1=2;
					int LA1_0 = input.LA(1);
					if ( (LA1_0=='-') ) {
						alt1=1;
					}
					switch (alt1) {
						case 1 :
							// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:12: '-'
							{
							match('-'); 
							}
							break;

					}

					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:17: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:18: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:28: ( '0' .. '9' )*
					loop2:
					while (true) {
						int alt2=2;
						int LA2_0 = input.LA(1);
						if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
							alt2=1;
						}

						switch (alt2) {
						case 1 :
							// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop2;
						}
					}

					}

					}
					break;
				case 2 :
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:239:43: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:240:7: ( 'true' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:240:9: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:241:7: ( 'false' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:241:9: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:242:7: ( '(' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:242:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:243:7: ( ')' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:243:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			int _type = CLPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:244:9: ( '{' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:244:11: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLPAR"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			int _type = CRPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:245:7: ( '}' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:245:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CRPAR"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:246:5: ( 'if' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:246:7: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:247:7: ( 'then' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:247:9: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:248:7: ( 'else' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:248:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:249:7: ( 'print' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:249:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "LET"
	public final void mLET() throws RecognitionException {
		try {
			int _type = LET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:250:5: ( 'let' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:250:7: 'let'
			{
			match("let"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LET"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			int _type = IN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:251:5: ( 'in' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:251:7: 'in'
			{
			match("in"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IN"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:252:5: ( 'var' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:252:7: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			int _type = FUN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:253:5: ( 'fun' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:253:7: 'fun'
			{
			match("fun"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:254:5: ( 'int' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:254:7: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			int _type = BOOL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:255:7: ( 'bool' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:255:9: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL"

	// $ANTLR start "ARROW"
	public final void mARROW() throws RecognitionException {
		try {
			int _type = ARROW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:256:9: ( '->' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:256:11: '->'
			{
			match("->"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARROW"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:258:5: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:258:7: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:259:5: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'Z')||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop4;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:261:10: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:261:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:261:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= '\t' && LA5_0 <= '\n')||LA5_0=='\r'||LA5_0==' ') ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:263:9: ( '/*' ( . )* '*/' )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:263:11: '/*' ( . )* '*/'
			{
			match("/*"); 

			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:263:16: ( . )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0=='*') ) {
					int LA6_1 = input.LA(2);
					if ( (LA6_1=='/') ) {
						alt6=2;
					}
					else if ( ((LA6_1 >= '\u0000' && LA6_1 <= '.')||(LA6_1 >= '0' && LA6_1 <= '\uFFFF')) ) {
						alt6=1;
					}

				}
				else if ( ((LA6_0 >= '\u0000' && LA6_0 <= ')')||(LA6_0 >= '+' && LA6_0 <= '\uFFFF')) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:263:16: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop6;
				}
			}

			match("*/"); 

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:265:10: ( . )
			// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:265:12: .
			{
			matchAny(); 
			 System.out.println("Invalid char: "+getText()); lexicalErrors++; _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:8: ( CLASS | EXTENDS | NEW | NULL | DOT | LE | GE | AND | OR | NOT | DIV | MINUS | SEMIC | COLON | COMMA | EQ | ASS | PLUS | TIMES | INTEGER | TRUE | FALSE | LPAR | RPAR | CLPAR | CRPAR | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | INT | BOOL | ARROW | ID | WHITESP | COMMENT | ERR )
		int alt7=41;
		alt7 = dfa7.predict(input);
		switch (alt7) {
			case 1 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:10: CLASS
				{
				mCLASS(); 

				}
				break;
			case 2 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:16: EXTENDS
				{
				mEXTENDS(); 

				}
				break;
			case 3 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:24: NEW
				{
				mNEW(); 

				}
				break;
			case 4 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:28: NULL
				{
				mNULL(); 

				}
				break;
			case 5 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:33: DOT
				{
				mDOT(); 

				}
				break;
			case 6 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:37: LE
				{
				mLE(); 

				}
				break;
			case 7 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:40: GE
				{
				mGE(); 

				}
				break;
			case 8 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:43: AND
				{
				mAND(); 

				}
				break;
			case 9 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:47: OR
				{
				mOR(); 

				}
				break;
			case 10 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:50: NOT
				{
				mNOT(); 

				}
				break;
			case 11 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:54: DIV
				{
				mDIV(); 

				}
				break;
			case 12 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:58: MINUS
				{
				mMINUS(); 

				}
				break;
			case 13 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:64: SEMIC
				{
				mSEMIC(); 

				}
				break;
			case 14 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:70: COLON
				{
				mCOLON(); 

				}
				break;
			case 15 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:76: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 16 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:82: EQ
				{
				mEQ(); 

				}
				break;
			case 17 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:85: ASS
				{
				mASS(); 

				}
				break;
			case 18 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:89: PLUS
				{
				mPLUS(); 

				}
				break;
			case 19 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:94: TIMES
				{
				mTIMES(); 

				}
				break;
			case 20 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:100: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 21 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:108: TRUE
				{
				mTRUE(); 

				}
				break;
			case 22 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:113: FALSE
				{
				mFALSE(); 

				}
				break;
			case 23 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:119: LPAR
				{
				mLPAR(); 

				}
				break;
			case 24 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:124: RPAR
				{
				mRPAR(); 

				}
				break;
			case 25 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:129: CLPAR
				{
				mCLPAR(); 

				}
				break;
			case 26 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:135: CRPAR
				{
				mCRPAR(); 

				}
				break;
			case 27 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:141: IF
				{
				mIF(); 

				}
				break;
			case 28 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:144: THEN
				{
				mTHEN(); 

				}
				break;
			case 29 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:149: ELSE
				{
				mELSE(); 

				}
				break;
			case 30 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:154: PRINT
				{
				mPRINT(); 

				}
				break;
			case 31 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:160: LET
				{
				mLET(); 

				}
				break;
			case 32 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:164: IN
				{
				mIN(); 

				}
				break;
			case 33 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:167: VAR
				{
				mVAR(); 

				}
				break;
			case 34 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:171: FUN
				{
				mFUN(); 

				}
				break;
			case 35 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:175: INT
				{
				mINT(); 

				}
				break;
			case 36 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:179: BOOL
				{
				mBOOL(); 

				}
				break;
			case 37 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:184: ARROW
				{
				mARROW(); 

				}
				break;
			case 38 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:190: ID
				{
				mID(); 

				}
				break;
			case 39 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:193: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 40 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:201: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 41 :
				// /Users/Giacomo/Desktop/FOOL_Giacomo/FOOL.g:1:209: ERR
				{
				mERR(); 

				}
				break;

		}
	}


	protected DFA7 dfa7 = new DFA7(this);
	static final String DFA7_eotS =
		"\1\uffff\3\42\1\uffff\4\40\1\56\1\60\3\uffff\1\66\4\uffff\2\42\4\uffff"+
		"\5\42\3\uffff\1\42\1\uffff\5\42\21\uffff\4\42\4\uffff\1\122\1\124\4\42"+
		"\1\uffff\3\42\1\134\1\42\1\136\3\42\1\142\1\uffff\1\143\1\uffff\1\42\1"+
		"\145\1\146\3\42\1\152\1\uffff\1\153\1\uffff\1\154\1\155\1\42\2\uffff\1"+
		"\42\2\uffff\1\160\1\161\1\42\4\uffff\1\163\1\164\2\uffff\1\42\2\uffff"+
		"\1\166\1\uffff";
	static final String DFA7_eofS =
		"\167\uffff";
	static final String DFA7_minS =
		"\1\0\2\154\1\145\1\uffff\2\75\1\46\1\174\1\52\1\61\3\uffff\1\75\4\uffff"+
		"\1\150\1\141\4\uffff\1\146\1\162\1\145\1\141\1\157\3\uffff\1\141\1\uffff"+
		"\1\164\1\163\1\167\1\154\1\164\21\uffff\1\165\1\145\1\154\1\156\4\uffff"+
		"\2\60\1\151\1\164\1\162\1\157\1\uffff\1\163\2\145\1\60\1\154\1\60\1\145"+
		"\1\156\1\163\1\60\1\uffff\1\60\1\uffff\1\156\2\60\1\154\1\163\1\156\1"+
		"\60\1\uffff\1\60\1\uffff\2\60\1\145\2\uffff\1\164\2\uffff\2\60\1\144\4"+
		"\uffff\2\60\2\uffff\1\163\2\uffff\1\60\1\uffff";
	static final String DFA7_maxS =
		"\1\uffff\1\154\1\170\1\165\1\uffff\2\75\1\46\1\174\1\52\1\76\3\uffff\1"+
		"\75\4\uffff\1\162\1\165\4\uffff\1\156\1\162\1\145\1\141\1\157\3\uffff"+
		"\1\141\1\uffff\1\164\1\163\1\167\1\154\1\164\21\uffff\1\165\1\145\1\154"+
		"\1\156\4\uffff\2\172\1\151\1\164\1\162\1\157\1\uffff\1\163\2\145\1\172"+
		"\1\154\1\172\1\145\1\156\1\163\1\172\1\uffff\1\172\1\uffff\1\156\2\172"+
		"\1\154\1\163\1\156\1\172\1\uffff\1\172\1\uffff\2\172\1\145\2\uffff\1\164"+
		"\2\uffff\2\172\1\144\4\uffff\2\172\2\uffff\1\163\2\uffff\1\172\1\uffff";
	static final String DFA7_acceptS =
		"\4\uffff\1\5\6\uffff\1\15\1\16\1\17\1\uffff\1\22\1\23\2\24\2\uffff\1\27"+
		"\1\30\1\31\1\32\5\uffff\1\46\1\47\1\51\1\uffff\1\46\5\uffff\1\5\1\6\1"+
		"\7\1\10\1\11\1\50\1\13\1\45\1\14\1\24\1\15\1\16\1\17\1\20\1\21\1\22\1"+
		"\23\4\uffff\1\27\1\30\1\31\1\32\6\uffff\1\47\12\uffff\1\33\1\uffff\1\40"+
		"\7\uffff\1\3\1\uffff\1\12\3\uffff\1\42\1\43\1\uffff\1\37\1\41\3\uffff"+
		"\1\35\1\4\1\25\1\34\2\uffff\1\44\1\1\1\uffff\1\26\1\36\1\uffff\1\2";
	static final String DFA7_specialS =
		"\1\0\166\uffff}>";
	static final String[] DFA7_transitionS = {
			"\11\40\2\37\2\40\1\37\22\40\1\37\5\40\1\7\1\40\1\25\1\26\1\20\1\17\1"+
			"\15\1\12\1\4\1\11\1\22\11\21\1\14\1\13\1\5\1\16\1\6\2\40\32\36\6\40\1"+
			"\36\1\35\1\1\1\36\1\2\1\24\2\36\1\31\2\36\1\33\1\36\1\3\1\36\1\32\3\36"+
			"\1\23\1\36\1\34\4\36\1\27\1\10\1\30\uff82\40",
			"\1\41",
			"\1\44\13\uffff\1\43",
			"\1\45\11\uffff\1\47\5\uffff\1\46",
			"",
			"\1\51",
			"\1\52",
			"\1\53",
			"\1\54",
			"\1\55",
			"\11\61\4\uffff\1\57",
			"",
			"",
			"",
			"\1\65",
			"",
			"",
			"",
			"",
			"\1\72\11\uffff\1\71",
			"\1\73\23\uffff\1\74",
			"",
			"",
			"",
			"",
			"\1\101\7\uffff\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"\1\106",
			"",
			"",
			"",
			"\1\110",
			"",
			"\1\111",
			"\1\112",
			"\1\113",
			"\1\114",
			"\1\115",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\116",
			"\1\117",
			"\1\120",
			"\1\121",
			"",
			"",
			"",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\12\42\7\uffff\32\42\6\uffff\23\42\1\123\6\42",
			"\1\125",
			"\1\126",
			"\1\127",
			"\1\130",
			"",
			"\1\131",
			"\1\132",
			"\1\133",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\1\135",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\1\137",
			"\1\140",
			"\1\141",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"",
			"\1\144",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\1\147",
			"\1\150",
			"\1\151",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\1\156",
			"",
			"",
			"\1\157",
			"",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\1\162",
			"",
			"",
			"",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			"",
			"",
			"\1\165",
			"",
			"",
			"\12\42\7\uffff\32\42\6\uffff\32\42",
			""
	};

	static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
	static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
	static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
	static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
	static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
	static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
	static final short[][] DFA7_transition;

	static {
		int numStates = DFA7_transitionS.length;
		DFA7_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
		}
	}

	protected class DFA7 extends DFA {

		public DFA7(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 7;
			this.eot = DFA7_eot;
			this.eof = DFA7_eof;
			this.min = DFA7_min;
			this.max = DFA7_max;
			this.accept = DFA7_accept;
			this.special = DFA7_special;
			this.transition = DFA7_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( CLASS | EXTENDS | NEW | NULL | DOT | LE | GE | AND | OR | NOT | DIV | MINUS | SEMIC | COLON | COMMA | EQ | ASS | PLUS | TIMES | INTEGER | TRUE | FALSE | LPAR | RPAR | CLPAR | CRPAR | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | INT | BOOL | ARROW | ID | WHITESP | COMMENT | ERR );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA7_0 = input.LA(1);
						s = -1;
						if ( (LA7_0=='c') ) {s = 1;}
						else if ( (LA7_0=='e') ) {s = 2;}
						else if ( (LA7_0=='n') ) {s = 3;}
						else if ( (LA7_0=='.') ) {s = 4;}
						else if ( (LA7_0=='<') ) {s = 5;}
						else if ( (LA7_0=='>') ) {s = 6;}
						else if ( (LA7_0=='&') ) {s = 7;}
						else if ( (LA7_0=='|') ) {s = 8;}
						else if ( (LA7_0=='/') ) {s = 9;}
						else if ( (LA7_0=='-') ) {s = 10;}
						else if ( (LA7_0==';') ) {s = 11;}
						else if ( (LA7_0==':') ) {s = 12;}
						else if ( (LA7_0==',') ) {s = 13;}
						else if ( (LA7_0=='=') ) {s = 14;}
						else if ( (LA7_0=='+') ) {s = 15;}
						else if ( (LA7_0=='*') ) {s = 16;}
						else if ( ((LA7_0 >= '1' && LA7_0 <= '9')) ) {s = 17;}
						else if ( (LA7_0=='0') ) {s = 18;}
						else if ( (LA7_0=='t') ) {s = 19;}
						else if ( (LA7_0=='f') ) {s = 20;}
						else if ( (LA7_0=='(') ) {s = 21;}
						else if ( (LA7_0==')') ) {s = 22;}
						else if ( (LA7_0=='{') ) {s = 23;}
						else if ( (LA7_0=='}') ) {s = 24;}
						else if ( (LA7_0=='i') ) {s = 25;}
						else if ( (LA7_0=='p') ) {s = 26;}
						else if ( (LA7_0=='l') ) {s = 27;}
						else if ( (LA7_0=='v') ) {s = 28;}
						else if ( (LA7_0=='b') ) {s = 29;}
						else if ( ((LA7_0 >= 'A' && LA7_0 <= 'Z')||LA7_0=='a'||LA7_0=='d'||(LA7_0 >= 'g' && LA7_0 <= 'h')||(LA7_0 >= 'j' && LA7_0 <= 'k')||LA7_0=='m'||LA7_0=='o'||(LA7_0 >= 'q' && LA7_0 <= 's')||LA7_0=='u'||(LA7_0 >= 'w' && LA7_0 <= 'z')) ) {s = 30;}
						else if ( ((LA7_0 >= '\t' && LA7_0 <= '\n')||LA7_0=='\r'||LA7_0==' ') ) {s = 31;}
						else if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\b')||(LA7_0 >= '\u000B' && LA7_0 <= '\f')||(LA7_0 >= '\u000E' && LA7_0 <= '\u001F')||(LA7_0 >= '!' && LA7_0 <= '%')||LA7_0=='\''||(LA7_0 >= '?' && LA7_0 <= '@')||(LA7_0 >= '[' && LA7_0 <= '`')||(LA7_0 >= '~' && LA7_0 <= '\uFFFF')) ) {s = 32;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 7, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
