grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
}

@lexer::members {
int lexicalErrors=0;
}

@members{
private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();

private int nestingLevel = -1;
//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
  
prog  returns [Node ast]
  :       e=exp SEMIC 
            {$ast = new ProgNode($e.ast);}
        | LET 
            {nestingLevel++;
             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
             symTable.add(hm);
            }
          d=declist IN e=exp SEMIC 
            {symTable.remove(nestingLevel--);
             $ast = new ProgLetInNode($d.astlist,$e.ast) ;} 
  ;
  

declist returns [ArrayList<Node> astlist]
  : {$astlist= new ArrayList<Node>() ;
     int offset=-2;}
        ( (
            VAR i=ID COLON t=type  ASS e=exp
              {VarNode v = new VarNode($i.text,$t.ast,$e.ast); 
               $astlist.add(v);
               HashMap<String,STentry> hm = symTable.get(nestingLevel); 
               if ( hm.put($i.text,new STentry(nestingLevel,$t.ast,offset--)) != null  ) // $t.ast
                 {System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
                  System.exit(0);}  
              }  
          | 
            FUN i=ID COLON t=type
              {//inserimento di ID nella symtable
               FunNode f = new FunNode($i.text,$t.ast);
               $astlist.add(f);
               HashMap<String,STentry> hm = symTable.get(nestingLevel);
              //nell'higherorder layout ogni unziona occupa un offset doppio!
               STentry entry = new STentry(nestingLevel,offset/*offset per l'higher order decrementato di due*/); //separo introducendo "entry"
               offset-=2;                
               if ( hm.put($i.text,entry) != null )
                 {System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
                  System.exit(0);}
                  //creare una nuova hashmap per la symTable
               nestingLevel++;
               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
               symTable.add(hmn);
               ArrayList<Node> parTypes = new ArrayList<Node>();
               int paroffset=1;
              }
              LPAR //
                (fid=ID COLON fty=type
                  { 
                  parTypes.add($fty.ast); //
                  ParNode fpar = new ParNode($fid.text,$fty.ast);
                  f.addPar(fpar);
                  //nel caso in cui ci siano paramtetri funzionali devo riservare due spazi
                  if($fty.ast instanceof ArrowTypeNode){
                  paroffset++;
                  }
                  
                  //se non è funzionale, semplicemente incremento di uno!
                  if ( hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset++)) != null  )
                    {System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
                     System.exit(0);}
                  }
                  (COMMA id=ID COLON ty=type
                    {
                      if($ty.ast instanceof ArrowTypeNode){ //come prima, se il parametro è funzionale, deve occupare due spazi  
                    
                       paroffset++;
                       
                        }
                    
                    parTypes.add($ty.ast); //
                    ParNode par = new ParNode($id.text,$ty.ast);
                    f.addPar(par);
                    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset++)) != null  )
                      {System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
                       System.exit(0);}
                    }
                  )*
                )? 
              RPAR {ArrowTypeNode functionType = new ArrowTypeNode(parTypes,$t.ast);
                entry.addType(functionType);
            // aggiungo il tipo anche al FunNode
              f.addSymType(functionType);} //
              (LET d=declist IN{f.addDec($d.astlist);})? e=exp 
              {//chiudere scope
              symTable.remove(nestingLevel--);
              f.addBody($e.ast);
              
              }
          ) SEMIC
        )+          
  ;
  

type returns [Node ast]   :  b=basic{$ast=$b.ast;}
        | a=arrow {$ast=$a.ast;} 
        ;

basic  returns [Node ast] : INT {$ast=new IntTypeNode();}              
        | BOOL {$ast=new BoolTypeNode();}                           
        ;  
    
 
arrow  returns [Node ast] : 
  {
    //lista dei parametri
    ArrayList<Node> parList = new ArrayList<Node>();
  }
  LPAR (t=type {parList.add($t.ast);} (COMMA t=type {parList.add($t.ast);})* )? RPAR ARROW b=basic
  // il tipo della funzione da ritornare
 {$ast = new ArrowTypeNode(parList,$b.ast);}
  ;          
       
exp returns [Node ast]
  : f=term {$ast= $f.ast;}
      (PLUS l=term
        {$ast= new PlusNode ($ast,$l.ast);}
        |  
        MINUS l2=term
        {$ast= new MinNode ($ast,$l2.ast);}
        | 
        OR l4=term {$ast=new OrNode($ast,$l4.ast);}
      )*
  ;
  
term  returns [Node ast]
  : f=factor {$ast= $f.ast;}
      (TIMES l=factor
        {$ast= new MultNode ($ast,$l.ast);}
        |
        DIV l2=factor
        {$ast= new DivNode ($ast,$l2.ast);}
        | 
        AND l5=factor {$ast=new AndNode($ast,$l5.ast);}
      )*
  ; 
  
factor  returns [Node ast]
  : f=value {$ast= $f.ast;}
      (EQ l=value 
        {$ast= new EqualNode ($ast,$l.ast);}
       
       | GE l3=value {$ast=new GreatEqNode($ast,$l3.ast);}
           
       | LE l2=value {$ast=new LessEqNode($ast,$l2.ast);}      
          )*
  ;   
  
value returns [Node ast]
  : n=INTEGER   
    {$ast= new IntNode(Integer.parseInt($n.text));}  
  | TRUE  
    {$ast= new BoolNode(true);}  
  | FALSE
    {$ast= new BoolNode(false);}  
  | LPAR e=exp RPAR
    {$ast= $e.ast;}  
  | IF x=exp THEN CLPAR y=exp CRPAR 
       ELSE CLPAR z=exp CRPAR 
    {$ast= new IfNode($x.ast,$y.ast,$z.ast);}  
  | NOT LPAR not=exp RPAR 
  {$ast=new NotNode($not.ast);}
    
  | PRINT LPAR e=exp RPAR 
    {$ast= new PrintNode($e.ast);}
  | i=ID 
    {//cercare la dichiarazione
    int j=nestingLevel;
    STentry entry=null; 
    
    while (j>=0 && entry==null){
      entry=(symTable.get(j--)).get($i.text);
     
      }
      
    if (entry==null)
      {System.out.println("Id "+$i.text+" at line "+$i.line+" not declared");
       System.exit(0);}               
    $ast= new IdNode($i.text,entry,nestingLevel);
   }
    //
    ( LPAR {ArrayList<Node> argList = new ArrayList<Node>();} //argList � l'arraylist dei parametri
      (fa=exp {argList.add($fa.ast);}
        (COMMA a=exp {argList.add($a.ast);})* 
      )?       
      RPAR {$ast=new CallNode($i.text,entry,argList,nestingLevel);}
       | DOT ID LPAR (exp (COMMA exp)* )? RPAR  )? 
 
    
         
  ; 

    
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
CLASS : 'class' ; 
EXTENDS : 'extends' ; 
NEW   : 'new' ; 
NULL    : 'null' ;
DOT: '.';
LE: '<=';
GE:'>=';
AND: '&&'; 
OR: '||';
NOT: 'not';
DIV: '/';
MINUS:'-'; 
SEMIC : ';' ;
COLON : ':' ;
COMMA : ',' ;
EQ  : '==' ;
ASS : '=' ;
PLUS  : '+' ;
TIMES : '*' ;
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE  : 'true' ;
FALSE : 'false' ;
LPAR  : '(' ;
RPAR  : ')' ;
CLPAR   : '{' ;
CRPAR : '}' ;
IF  : 'if' ;
THEN  : 'then' ;
ELSE  : 'else' ;
PRINT : 'print' ; 
LET : 'let' ;
IN  : 'in' ;
VAR : 'var' ;
FUN : 'fun' ;
INT : 'int' ;
BOOL  : 'bool' ;
ARROW   : '->' ; 

ID  : ('a'..'z'|'A'..'Z')
    ('a'..'z'|'A'..'Z'|'0'..'9')* ;

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

COMMENT : '/*' .* '*/' { $channel=HIDDEN; } ;
 
ERR      : . { System.out.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ; 

